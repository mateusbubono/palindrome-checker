# Palindrome Checker

Cette Page web utilise JavaScript pour vérifier si un mot est un palindrome (c'est à dire qu'il s'écrit de la même façon à l'endroit comme à l'envers => anna).

## Installation

vous pouvez cloner le projet en utilisant git clone

```bash
git clone https://gitlab.com/mateusbubono/palindrome-checker.git
```

## Usage

Après avoir cloner le projet vous pouvez ouvrir la page web via index.html ou en vous rendant à l'adresse suivante :

https://mateusbubono.gitlab.io/palindrome-checker/
