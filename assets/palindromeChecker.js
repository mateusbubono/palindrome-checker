/**
 * @file palindromeChecker.js
 * @author Boris Merminod
 * @version 1.0
 *
 * Le script permet d'analyser un mot et de déduire s'il s'agit d'un
 * palindrome (un mot qui à la même orthographe quand il est écrit
 * à l'envers), puis d'afficher le résultat dans la page html.
 * Pour ce faire le script utilises les fonctions suivantes :
 * - reverseString(str) => prend une string en paramètre et inverse
 * les caractères
 *
 * - cleanString(str) => prend une string en paramètre et retire tous
 * les caractères non alphanumériques
 *
 * - isPalindrome(str) => prend une string en paramètre la traite
 * grâce à cleanString et reverseString et compare avec la chaîne
 * d'origine pour savoir s'il s'agit d'un palindrome
 *
 * - palindromeProcessing() => Intéragit avec la page html et lance le
 * traitement de la string entrée par l'utilisateur. La page html est
 * mise à jour en fonction du résultat traité  
 *
 * - main() => attache un eventListener au bouton de la page html pour
 * lancé le traitement de la string entrée par l'utilisateur
 */


/**
* fonction reverseString : inverse le sens d'écriture d'une string puis retourne le résultat.
* La fonction prend une string en paramètre mais peut prendre tout type de donnée. Si le paramètre passé est un objet, il sera remplacé par une 
* string vide
*
* @param {String} str : string en entrée à inverser
*
* @return {String} : Retourne la string en entrée en sens inversée
*/
function reverseString(str)
{
	if(typeof str != "string")
	{
		str = (typeof str == "object") ? ((Array.isArray(str))? "" + str : "") : "" + str
	}
	
	let strArr = str.split("");
	strArr = strArr.reverse();
	return strArr.join("");
}

/**
* fonction cleanString : La fonction va changer toutes la majuscules en minuscule d'une string. Ensuite elle va supprimer tout caractères qui
* n'est ni une lettre ni un nombre. La fonction retourne la string transformée en résultat. La fonction peut prendre tout type de données en
* paramètre. S'il s'agit d'un objet il est alors remplacé par une string vide
*
* @param {String} str : string en entrée à nettoyer
*
* @return {String} : La string nettoyée
*/
function cleanString(str)
{
	if(typeof str != "string")
	{
		str = (typeof str == "object") ? ((Array.isArray(str))? "" + str : "") : "" + str
	}
	
	return str.toLowerCase().replace(/[\W+_]/g, "")
}

/**
* fonction isPalindrome : Compare une string et sa version inversée pour savoir s'il s'agit d'un palindrome. La string est nettoyée avant d'être
* inversée puis comparée
*
*@param {String} str : La string à analyser
*
*@return {Boolean} : Si la string est un palindrome la fonction retourne true. false le cas échant
*/
function isPalindrome(str)
{
	const cleanedStr = cleanString(str)
	return cleanedStr == reverseString(cleanedStr)
}


/**
* fonction palindromeProcessing : La fonction se charge de récupérer
* certains éléments de la page html :
* - Un champs input qui récupère le mot à traiter => input-field
* - Un champs paragraphe qui va afficher le mot nettoyé des caractères
* non traités => output-field
* - Un champs paragraphe qui va afficher le mot nettoyé et inversé =>
* word-output-field
* - Un champs paragraphe qui va indiquer si le mot analysé est un
* palindrome => palindrome-output-field
*
* La fonction va ensuite passer le mot à analysé aux fonctions
* reverseString et cleanString puis afficher le résultat dans
* les paragraphes de la page html prévue à cet effet
*/
function palindromeProcessing()
{
	const inputField = document.getElementById("input-field")
	const outputField = document.getElementById("output-field")
	const wordOutputField = document.getElementById("word-output-field")
	const palindromeOutputField = document.getElementById("palindrome-output-field")
	
	if(inputField != null && outputField != null && wordOutputField != null  && palindromeOutputField != null)
	{
		outputField.textContent = cleanString(inputField.value)
		wordOutputField.textContent = reverseString(cleanString(inputField.value))
		palindromeOutputField.textContent = isPalindrome(inputField.value) ? "Il est donc un palindrome" : "Il n'est donc pas un palindrome"
	}
	
	
}

/*
* fonction main() va simplement ajouter un eventListener au bouton
* Envoyer de la page html pour déclencher l'analyse du mot entré dans 
* le champs textuel
*/
function main()
{
	let button = document.getElementById("send-button")
	if(button != null)
	{
		button.addEventListener("click",  palindromeProcessing)
	}
}

palindromeProcessing()
main()


