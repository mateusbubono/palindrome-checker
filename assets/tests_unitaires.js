QUnit.test("test => reverseString(str)", () => {
	QUnit.assert.equal(reverseString("anna"), "anna", "reverseString() => param : 'anna'");
	QUnit.assert.equal(reverseString("eye"), "eye", "reverseString() => param : 'eye'");
	QUnit.assert.notEqual(reverseString("_eye"), "_eye" , "reverseString() => param : '_eye'");
	QUnit.assert.notEqual(reverseString("race car"), "race car" , "reverseString() => param : 'race car'");
	QUnit.assert.notEqual(reverseString("not a palindrome"), "not a palindrome" , "reverseString() => param : 'not a palindrome'");
	QUnit.assert.notEqual(reverseString("A man, a plan, a canal. Panama"), "A man, a plan, a canal. Panama" , "reverseString() => param : 'A man, a plan, a canal. Panama'");
	QUnit.assert.notEqual(reverseString("never odd or even"), "never odd or even" , "reverseString() => param : 'never odd or even'");
	QUnit.assert.notEqual(reverseString("nope"), "nope" , "reverseString() => param : 'nope'");
	QUnit.assert.notEqual(reverseString("almostomla"), "almostomla" , "reverseString() => param : 'almostomla'");
	QUnit.assert.notEqual(reverseString("My age is 0, 0 si ega ym"), "My age is 0, 0 si ega ym" , "reverseString() => param : 'My age is 0, 0 si ega ym'");
	QUnit.assert.notEqual(reverseString("1 eye for of 1 eye."), "1 eye for of 1 eye." , "reverseString() => param : '1 eye for of 1 eye.'");
	QUnit.assert.notEqual(reverseString("0_0 (: /-\ :) 0-0"), "0_0 (: /-\ :) 0-0" , "reverseString() => param : '0_0 (: /-\ :) 0-0'");
	QUnit.assert.notEqual(reverseString("five|\_/|four"), "five|\_/|four" , "reverseString() => param : 'five|\_/|four'");
	QUnit.assert.equal(reverseString(101), "101" , "reverseString() => param : 101");
	QUnit.assert.equal(reverseString(1.01), "10.1" , "reverseString() => param : 1.01");
	QUnit.assert.equal(reverseString(true), "eurt" , "reverseString() => param : true");
	QUnit.assert.equal(reverseString([1,2,3]), "3,2,1" , "reverseString() => param : [1,2,3]");
	QUnit.assert.equal(reverseString({nom: "Bubono", prenom: "Mateus"}), "" , "reverseString() => param : {nom: 'Bubono', prenom: 'Mateus'}");
})

QUnit.test("test => cleanString(str)", () => {
	QUnit.assert.equal(cleanString("anna"), "anna", "cleanString() => param : 'anna'");
	QUnit.assert.equal(cleanString("eye"), "eye", "cleanString() => param : 'eye'");
	QUnit.assert.equal(cleanString("_eye"), "eye" , "cleanString() => param : '_eye'");
	QUnit.assert.equal(cleanString("race car"), "racecar" , "cleanString() => param : 'race car'");
	QUnit.assert.equal(cleanString("not a palindrome"), "notapalindrome" , "cleanString() => param : 'not a palindrome'");
	QUnit.assert.equal(cleanString("A man, a plan, a canal. Panama"), "amanaplanacanalpanama" , "reverseString() => param : 'A man, a plan, a canal. Panama'");
	QUnit.assert.equal(cleanString("never odd or even"), "neveroddoreven" , "cleanString() => param : 'never odd or even'");
	QUnit.assert.equal(cleanString("nope"), "nope" , "cleanString() => param : 'nope'");
	QUnit.assert.equal(cleanString("almostomla"), "almostomla" , "cleanString() => param : 'almostomla'");
	QUnit.assert.equal(cleanString("My age is 0, 0 si ega ym"), "myageis00siegaym" , "cleanString() => param : 'My age is 0, 0 si ega ym'");
	QUnit.assert.equal(cleanString("1 eye for of 1 eye."), "1eyeforof1eye" , "cleanString() => param : '1 eye for of 1 eye.'");
	QUnit.assert.equal(cleanString("0_0 (: /-\ :) 0-0"), "0000" , "cleanString() => param : '0_0 (: /-\ :) 0-0'");
	QUnit.assert.equal(cleanString("five|\_/|four"), "fivefour" , "cleanString() => param : 'five|\_/|four'");
	QUnit.assert.equal(cleanString(101), "101" , "cleanString() => param : 101");
	QUnit.assert.equal(cleanString(1.01), "101" , "cleanString() => param : 1.01");
	QUnit.assert.equal(cleanString(true), "true" , "cleanString() => param : true");
	QUnit.assert.equal(cleanString([1,2,3]), "123" , "cleanString() => param : [1,2,3]");
	QUnit.assert.equal(cleanString({nom: "Bubono", prenom: "Mateus"}), "" , "cleanString() => param : {nom: 'Bubono', prenom: 'Mateus'}");
})

QUnit.test("test => cleanString(str)", () => {
	QUnit.assert.equal(cleanString("anna"), "anna", "cleanString() => param : 'anna'");
	QUnit.assert.equal(cleanString("eye"), "eye", "cleanString() => param : 'eye'");
	QUnit.assert.equal(cleanString("_eye"), "eye" , "cleanString() => param : '_eye'");
	QUnit.assert.equal(cleanString("race car"), "racecar" , "cleanString() => param : 'race car'");
	QUnit.assert.equal(cleanString("not a palindrome"), "notapalindrome" , "cleanString() => param : 'not a palindrome'");
	QUnit.assert.equal(cleanString("A man, a plan, a canal. Panama"), "amanaplanacanalpanama" , "reverseString() => param : 'A man, a plan, a canal. Panama'");
	QUnit.assert.equal(cleanString("never odd or even"), "neveroddoreven" , "cleanString() => param : 'never odd or even'");
	QUnit.assert.equal(cleanString("nope"), "nope" , "cleanString() => param : 'nope'");
	QUnit.assert.equal(cleanString("almostomla"), "almostomla" , "cleanString() => param : 'almostomla'");
	QUnit.assert.equal(cleanString("My age is 0, 0 si ega ym"), "myageis00siegaym" , "cleanString() => param : 'My age is 0, 0 si ega ym'");
	QUnit.assert.equal(cleanString("1 eye for of 1 eye."), "1eyeforof1eye" , "cleanString() => param : '1 eye for of 1 eye.'");
	QUnit.assert.equal(cleanString("0_0 (: /-\ :) 0-0"), "0000" , "cleanString() => param : '0_0 (: /-\ :) 0-0'");
	QUnit.assert.equal(cleanString("five|\_/|four"), "fivefour" , "cleanString() => param : 'five|\_/|four'");
	QUnit.assert.equal(cleanString(101), "101" , "cleanString() => param : 101");
	QUnit.assert.equal(cleanString(1.01), "101" , "cleanString() => param : 1.01");
	QUnit.assert.equal(cleanString(true), "true" , "cleanString() => param : true");
	QUnit.assert.equal(cleanString([1,2,3]), "123" , "cleanString() => param : [1,2,3]");
	QUnit.assert.equal(cleanString({nom: "Bubono", prenom: "Mateus"}), "" , "cleanString() => param : {nom: 'Bubono', prenom: 'Mateus'}");
})


QUnit.test("test => isPalindrome(str)", () => {
	QUnit.assert.equal(isPalindrome("anna"), true, "isPalindrome() => param : 'anna'");
	QUnit.assert.equal(isPalindrome("eye"), true, "isPalindrome() => param : 'eye'");
	QUnit.assert.equal(isPalindrome("_eye"), true , "isPalindrome() => param : '_eye'");
	QUnit.assert.equal(isPalindrome("race car"), true , "isPalindrome() => param : 'race car'");
	QUnit.assert.equal(isPalindrome("not a palindrome"), false , "isPalindrome() => param : 'not a palindrome'");
	QUnit.assert.equal(isPalindrome("A man, a plan, a canal. Panama"), true , "isPalindrome() => param : 'A man, a plan, a canal. Panama'");
	QUnit.assert.equal(isPalindrome("never odd or even"), true , "isPalindrome() => param : 'never odd or even'");
	QUnit.assert.equal(isPalindrome("nope"), false, "cleanString() => param : 'nope'");
	QUnit.assert.equal(isPalindrome("almostomla"), false , "isPalindrome() => param : 'almostomla'");
	QUnit.assert.equal(isPalindrome("My age is 0, 0 si ega ym"), true , "isPalindrome() => param : 'My age is 0, 0 si ega ym'");
	QUnit.assert.equal(isPalindrome("1 eye for of 1 eye."), false , "isPalindrome() => param : '1 eye for of 1 eye.'");
	QUnit.assert.equal(isPalindrome("0_0 (: /-\ :) 0-0"), true , "isPalindrome() => param : '0_0 (: /-\ :) 0-0'");
	QUnit.assert.equal(isPalindrome("five|\_/|four"), false , "isPalindrome() => param : 'five|\_/|four'");
	QUnit.assert.equal(isPalindrome(101), true , "isPalindrome() => param : 101");
	QUnit.assert.equal(isPalindrome(1.01), true , "isPalindrome() => param : 1.01");
	QUnit.assert.equal(isPalindrome(true), false , "isPalindrome() => param : true");
	QUnit.assert.equal(isPalindrome([1,2,3]), false , "isPalindrome() => param : [1,2,3]");
	QUnit.assert.equal(isPalindrome({nom: "Bubono", prenom: "Mateus"}), true , "isPalindrome() => param : {nom: 'Bubono', prenom: 'Mateus'}");
})

